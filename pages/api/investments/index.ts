import type { NextApiRequest, NextApiResponse } from 'next'
import {isValidMethod} from "@/helpers/api";
import fetch from "node-fetch";

type Status = {
    success: boolean
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Object|Status>,
): Promise<void> {
    if (!isValidMethod(req.method as string, 'POST')) {
        return res.status(405).send({ success: false })
    }
    const body = req.body
    const backendURL = `${process.env['BACKEND_URL']}`
    if (body && backendURL && backendURL == '') {
        return res.status(400).send({message: "Could not connect to API"})
    }

    let payload
    if (typeof body != "string"){ //todo: we can also check if the body has the appropriate params
        payload = JSON.stringify(body);
    } else {
        payload = body
    }

    const url = `${backendURL}/user/add/fund`
    try {
        const postData = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: payload,
        }

        const response = await fetch(url, postData);
        if (response.status == 401) {
            return res.status(401).send({ success: false, message: res.statusMessage })
        }

        if (response.status == 201) {
            return res.status(201).send({ success: false, message: res.statusMessage })
        }

        return res.status(response.status).send({ success: false })
    } catch (error) {
        return res.status(500).send({ success: false })
    }
}
