import {Box, Center, Stack, Text} from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
import { CustomerForm } from "@/components/customer-form";
import Toggle from "@/components/toggle";


export default function Home() {
    return(
        <>
            <Toggle></Toggle>
            <Center h="100vh">
                <Stack>
                    <Box className={"bg-gray-800 dark:bg-white"} p={8} rounded="md" shadow="lg">
                        <CustomerForm></CustomerForm>
                    </Box>
                </Stack>
            </Center>
        </>
  )
}
