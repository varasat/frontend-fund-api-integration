export function isValidMethod(
    requestMethod: string,
    proxyMethod: string | string[],
): boolean {
    if (Array.isArray(proxyMethod)) {
        return proxyMethod.includes(requestMethod)
    }

    return requestMethod === proxyMethod
}
