import {
    Button,
    NumberInput,
    NumberInputField,
    FormControl,
    FormErrorMessage,
    FormLabel,
    Input,
    Text
} from "@chakra-ui/react";
import {FC, FormEvent, useState} from "react";

export interface CustomerFormProps {
    userId?: string
    fundId?: number
    amount?: number
}


export const CustomerForm: FC<CustomerFormProps> = ({userId = "",fundId, amount})=>{
    const [inputUserId, setInputUserId] = useState(userId)
    const [inputFundId, setFundId] = useState(fundId)
    const [inputAmount, setInputAmount] = useState(amount)
    const [error, setError] = useState("")
    const [successMessage, setSuccessMessage] = useState('');

    const [isDirty, setIsDirty] = useState(false)

    const handleInputChange = (e:any) => {
        console.log(e.target.name)
        switch (e.target.name) {
            case "user_id":
                setInputUserId(e.target.value)
                break;
            case "fund_id":
                setFundId(e.target.value)
                break;
            case "amount":
                setInputAmount(e.target.value)
                break;
        }
        setIsDirty(true)
    }

    async function onSubmit(e: FormEvent<HTMLFormElement>) {
        e.preventDefault()

        const formData = new FormData(e.currentTarget)

        const body = {
            user_id: formData.get('user_id') ?? '',
            // @ts-ignore
            fund_id: parseInt(formData.get('fund_id') ?? '0', 10),
            // @ts-ignore
            amount: parseFloat(formData.get('amount') ?? '0.0'), // Default to 0.0 if amount is not found
        }

        const response = await fetch('/api/investments', {
            method: 'POST',
            body: JSON.stringify(body),
        })

        if(response.status == 401){
            setError("This user cannot make more investments")
            setSuccessMessage("")
        }

        if(response.status == 201){
            setSuccessMessage("Investment successful")
            setError("")
        }
    }

    const isError = inputUserId === ''
    return (
        <form onSubmit={(event) => onSubmit(event) }>
            <FormControl className="text-white dark:text-black" isInvalid={isError && isDirty}>
                <FormLabel className="text-white dark:text-black">Customer ID</FormLabel>
                <Input name="user_id" className="text-white dark:text-black" value={inputUserId} onChange={handleInputChange} />
                <FormLabel className="mt-6 text-white dark:text-black">Fund ID</FormLabel>
                <Input name="fund_id" className="text-white dark:text-black" value={inputFundId} onChange={handleInputChange} />
                <FormLabel className="mt-6 text-white dark:text-black">Amount</FormLabel>
                <NumberInput><NumberInputField name="amount" className="text-white dark:text-black" value={inputAmount} onChange={handleInputChange} /></NumberInput>
                <FormErrorMessage>Please fill in all the fields</FormErrorMessage>
            </FormControl>
            <Button className="mt-6" type="submit">Submit</Button>
            {successMessage != "" && <Text className="text-lime-400">{successMessage}</Text>}
            {error != "" && <Text className="text-red-500">{error}</Text>}
        </form>
    )
}