# nextjs-basic-project

Hello! this is Andrei, had a lot of fun with this project.
I've forked one of my frontend skeleton projects for this and added the following:
- an api route in pages/api/investments that calls our backend. This is done for:
  - Server Side Data fetching
  - If we want to add authentication at a later date it's more secure doing it on the server side to protect access to sensitive endpoints and data
  - API routes would be quite easy to test and debug and write mocks

- an index page with the following features:
  - Dark/light mode
  - A form that uses both Chakra UI and Tailwind
  - when providing the form the appropriate data it allows the user to invest in a fund by calling the appropriate api
- 

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Also please run the backend concurrently so that the Next API can call something. 
You can find the Backend API [here](https://gitlab.com/varasat/api-usecase-number-1-fund-and-users/)

## Features & To-dos
 
- [X] Add next.js
- [X] Add Tailwind 
- [X] Add Chakra UI
- [X] Create a couple of components
- [X] Add Dark Mode
- [X] Add an Api route either to a local server or mocked
- [ ] Add Jest 
- [ ] Add Coverage
- [ ] Fix EsLint
 
A lot more todos on here. This is just a light example that shows I can develop in both React and Go
Ideally I'd love to fix Eslint for this project

## Status
Active 